function ChoicesHandler(core) {
    this.core = core;

    this.handlers = new Map();

    this.registerChoiceHandler('exit', ()=>this.core.closeApp());
    this.registerChoiceHandler('printLog', ()=>{
        console.log(this.core.logStore);
        this.core.mainPage();
    });
}

ChoicesHandler.prototype.registerChoiceHandler = function (choice, callBack) {
    this.handlers.set(choice, callBack);
};
ChoicesHandler.prototype.unRegisterChoiceHandler = function (choice) {
    this.handlers.delete(choice);
};

ChoicesHandler.prototype.handleChoice = function (choice) {
    if(this.handlers.has(choice)){
        return this.handlers.get(choice)();
    }else{
        console.log(`choice handler not found: ${choice}`);
        this.core.mainPage();
    }
};

module.exports = core=>new ChoicesHandler(core);