let choices = require('./choicesStore');

function ChoicesManager(choices) {
    this.choices = choices
}

ChoicesManager.prototype.registerChoice = function (choice) {
    this.choices.push(choice)
};
ChoicesManager.prototype.unRegisterChoice = function (choice) {
    for(let id in this.choices){
        if(this.choices[id].name === choice){
            this.choices.splice(id, 1);
        }
    }
}

ChoicesManager.prototype.getChoices = function () {
    return choices
};

module.exports = ()=>new ChoicesManager(choices);