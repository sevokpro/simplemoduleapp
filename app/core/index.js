const choiceManager = require('./choicesManager')();
const choiceHandler = require('./choiceHandler');

const {prompt} = require('inquirer');

function AppCore(choiceManager, choiceHandler){
    this.choiceManager = choiceManager;
    this.logStore = [];
    this.choiceHandler = choiceHandler(this);
}

AppCore.prototype.print = function (data) {
    console.log(data);
};

AppCore.prototype.mainPage = function () {
    let customChoices = choiceManager.getChoices();
    let resultChoices = customChoices.concat([{
        name: 'exit',
        value: 'exit'
    }]);

    prompt([{
        type: 'list',
        name: 'answer',
        message: 'welcome to modular app',
        choices: resultChoices
    }]).then(answer=>{
        this.choiceHandler.handleChoice(answer['answer']);
    });
};

AppCore.prototype.run = function () {
    this.appClose = new Promise(resolve=>{
        this.closeApp = ()=>resolve()
    });

    this.mainPage();

    return this.appClose;
};

module.exports = ()=>new AppCore(choiceManager, choiceHandler);

