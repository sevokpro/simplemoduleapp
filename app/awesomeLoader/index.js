const {readdir, readFile} = require('fs');

function AwesomeModuleLoader(initParams) {
    this.basePath = `${initParams.path}`;
    this.manifestCache = new Map();
}

/**
 * @private
 * @returns {Promise}
 */
AwesomeModuleLoader.prototype.getManifests = function () {
    return new Promise(
        (resolve, reject)=>{
            readdir(`${this.basePath}`, (err, result)=>{
                let manifests = [];
                for ( let item of result ){
                    if(this.checkManifestCache(item)){
                        manifests.push(this.getManifestFromCache(item))
                    }else{
                        manifests.push(this.readManifest(item))
                    }
                }
                Promise.all(manifests).then(subResolve=>resolve(subResolve))
            });
        }
    )
};

/**
 * @private
 * @param folderName
 * @returns {Promise}
 */
AwesomeModuleLoader.prototype.readManifest = function (folderName) {
    return new Promise(resolve=>{
        readFile(`${this.basePath}/${folderName}/manifest.json`, {encoding: 'utf8'}, (err, result)=>{
            let manifest = JSON.parse(result);
            manifest.folder = folderName;
            this.manifestCache.set(folderName, manifest);
            resolve(this.manifestCache.get(folderName));
        })
    })
};

/**
 * @private
 * @param folderName
 * @returns {V}
 */
AwesomeModuleLoader.prototype.getManifestFromCacheSync = function (folderName){
    return this.manifestCache.get(folderName)
};

/**
 * @private
 * @param folderName
 * @returns {Promise}
 */
AwesomeModuleLoader.prototype.getManifestFromCache = function (folderName) {
    return new Promise(resolve=>{
        resolve(this.getManifestFromCacheSync(folderName))
    })
};

/**
 * @private
 * @param folderName
 * @returns {boolean}
 */
AwesomeModuleLoader.prototype.checkManifestCache = function (folderName) {
    return this.manifestCache.has(folderName);
};

/**
 * @public
 * @returns {Promise}
 */
AwesomeModuleLoader.prototype.getAvailableModules = function () {
    return new Promise(resolve=>{
        this.getManifests().then(
            result=>{
                let filterResult = [];
                for(let manifest of result){
                    filterResult.push({
                        verboseName: `${manifest.name}@${manifest.version}`,
                        uuid: manifest.uuid,
                        version: manifest.version,
                        basePath: manifest.basePath
                    })
                }
                resolve(filterResult)
            }
        )
    })
};

AwesomeModuleLoader.prototype.loadModule = function (uuid) {
    return new Promise(resolve=>{
        this.getManifests().then(
            result=>{
                for (let manifest of result){
                    if(manifest.uuid === uuid){
                        resolve(require(`${this.basePath}/${manifest.folder}/${manifest.basePath}`));
                        break;
                    }
                }
            }
        )
    })
};
module.exports = (initParams)=>new AwesomeModuleLoader(initParams);