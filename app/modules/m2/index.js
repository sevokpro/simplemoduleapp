function M2Module(config) {
    this.appHost = config.appHost;
}

M2Module.prototype.register = function () {
    return new Promise(resolve=>{
        this.appHost.executeCommand('log', 'm2 registred');
        this.appHost.executeCommand('registerMenuItem', {name: 'm2ModuleMenu', cbCommand: 'm2Callback'});
        this.appHost.executeCommand('registerChoiceCallback', {answer: 'm2Callback', callback: ()=>{
            this.appHost.executeCommand('sendMessageToUser', 'hello user! from m2');
            this.appHost.executeCommand('goToMainPage')
        }});
        resolve()
    })
};

M2Module.prototype.unRegister = function () {
    return new Promise(resolve=>{
        this.appHost.executeCommand('log', 'm2 unregister');
        this.appHost.executeCommand('unRegisterMenuItem', 'm2ModuleMenu');
        this.appHost.executeCommand('unRegisterChoiceCallback', 'm2Callback');
        resolve()
    })
};

module.exports = M2Module;