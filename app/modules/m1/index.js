function M1Module(config) {
    this.appHost = config.appHost;
}

M1Module.prototype.register = function () {
    return new Promise(resolve=>{
        this.appHost.executeCommand('log', 'm1 registred');
        this.appHost.executeCommand('registerMenuItem', {name: 'm1ModuleMenu', cbCommand: 'm1Callback'});
        this.appHost.executeCommand('registerChoiceCallback', {answer: 'm1Callback', callback: ()=>{
            this.appHost.executeCommand('sendMessageToUser', 'hello user! from m1');
            this.appHost.executeCommand('goToMainPage')
        }});
        resolve()
    })
};

M1Module.prototype.unRegister = function () {
    return new Promise(resolve=>{
        this.appHost.executeCommand('log', 'm1 unregister');
        this.appHost.executeCommand('unRegisterMenuItem', 'm1ModuleMenu');
        this.appHost.executeCommand('unRegisterChoiceCallback', 'm1Callback');
        resolve()
    })
};

module.exports = M1Module;