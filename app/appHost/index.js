function CoreHost(core) {
    this.core = core;
    this.moduleStorage = new Map();
}

CoreHost.prototype.version = '1.0.0';

CoreHost.prototype.executeCommand = function (command, args) {
    switch (command){
        case 'runApp':
            return this.core.run();
            break;
        case 'log':
            return this.core.logStore.push(args);
        case 'registerMenuItem':
            return this.core.choiceManager.registerChoice({name: args.name, value: args.cbCommand});
        case 'registerChoiceCallback':
            return this.core.choiceHandler.registerChoiceHandler(args.answer, args.callback);
        case 'sendMessageToUser':
            return this.core.print(args);
        case 'goToMainPage':
            return this.core.mainPage();
        case 'unRegisterMenuItem':
            return this.core.choiceManager.unRegisterChoice(args);
        case'unRegisterChoiceCallback':
            return this.core.choiceHandler.unRegisterChoiceHandler(args);
        default:
            console.log(`command not support: ${command}, args: ${JSON.stringify(args)}`);
    }
};

CoreHost.prototype.registerModule = function (module) {
    return new Promise(resolve=>{
        let moduleInstance = new module.moduleConstructor({appHost: this});
        moduleInstance.register().then(
            success=>{
                this.moduleStorage.set(module.uuid, moduleInstance);
                resolve(success);
            },
            reject=>{
                reject(`init module break reject: ${reject}`);
            }
        );
    })
};
CoreHost.prototype.unRegisterModule = function (moduleUuid) {
    return new Promise((resolve, reject)=>{
        let module = this.moduleStorage.get(moduleUuid);
        module.unRegister().then(
            resolve=>{
                this.moduleStorage.delete(moduleUuid);
            },
            error=>{
                reject(error);
            }
        )
    })
};
CoreHost.prototype.updateModule = function (module) {
    return new Promise((resolve, reject)=>{
        let cachedModule = this.getModule(module.uuid);
        this.unRegisterModule(module.uuid);
        this.registerModule(module).then(
            success=>{
                resolve(success)
            },
            error=>{
                this.registerModule(cachedModule).then(
                    success=>{
                        reject(error)
                    },
                    criticalError=>{
                        reject(`Critical error when rollback update! ${criticalError}`)
                    }
                )
            }
        )
    })

};
CoreHost.prototype.getModule = function (moduleUuid) {
    return this.moduleStorage.get(moduleUuid);
};

CoreHost.prototype.updateModules = function (modules) {
    return new Promise(resolve=>{
        let resultPromise = [];
        let currentModules = new Map();
        for(let key of this.moduleStorage.keys()){
            currentModules.set(key, true)
        }

        for(let module of modules){
            if(currentModules.has(module.uuid)){
                resultPromise.push(this.updateModule(module));
                currentModules.set(module.uuid, false);
            }else{
                resultPromise.push(this.registerModule(module));
            }
        }
        for(let [key, val] of currentModules.entries()){
            if(val === true){
                this.unRegisterModule(key);
            }
        }
        Promise.all(resultPromise).then(
            success=>{
                resolve();
            }
        )
    })
};

module.exports = core=>new CoreHost(core);