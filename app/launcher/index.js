const core = require('../core')();
const appHost = require('../appHost')(core);
const moduleLoader = require('../awesomeLoader')({path: `${__dirname}/../modules`, hostAppVersion: appHost.version});
const loaderCli = require('../awesomeLoaderCLI');
const {prompt} = require('inquirer');
const queryId = `answer`;

function main() {
    prompt([{
        type: 'list',
        name: queryId,
        message: 'welcome to app launcher',
        choices: [
            {name: 'run app', value: ()=>{
                appHost.executeCommand('runApp').then(
                    appClosed=>main()
                )
            }},
            {name: 'configurator', value: ()=>{
                loaderCli.run({baseModuleDir: `${__dirname}/../modules`}).then(
                    checkedModules=>{
                        appHost.updateModules(checkedModules).then(
                            resolve=>main(),
                            reject=>main()
                        )
                    }
                )
            }},
            {name: 'exit', value: ()=>console.log(`Goodbye!`)}
        ]
    }]).then(answer=>answer[queryId]());

}
main();