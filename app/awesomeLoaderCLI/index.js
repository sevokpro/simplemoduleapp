const process = require('process')
const mainMenu = require('./menu/mainMenu');
let glob = require('./globalScope');

module.exports = {
    run: function (initParams) {
        glob.set('moduleLoader', require('../awesomeLoader')({path: initParams.baseModuleDir}));
        mainMenu();
        return new Promise(resolve=>glob.set('closeApp', resolve));
    }
};