const {prompt} = require('inquirer');
const queryId = 'prompt';
const {separate} = require('./../separator');

const exit = require('./nodes/exit');
const selectAppModules = require('./nodes/selectAppModules');

module.exports = ()=>{
    separate();
    prompt([{
        type: 'list',
        name: queryId,
        message: 'welcome to module app configurator!',
        choices: [
            {name: 'select app modules', value: selectAppModules},
            {name: 'exit', value: exit}
        ]
    }]).then(answer=>answer[queryId]({back: module.exports}));
};