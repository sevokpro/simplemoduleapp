const {prompt} = require('inquirer');
const glob = require('./../../globalScope');
const {separate} = require('./../../separator');
const queryId = 'moduleUUID';
const fs = require('fs');
const exit = require('./exit');

module.exports = ()=>{
    const moduleLoader = glob.get('moduleLoader');
    const appManifestPath = glob.get('appManifest');

    moduleLoader.getAvailableModules().then(
        resolve=>{
            separate();
            let choices = [];
            for(let module of resolve){
                choices.push({
                    name: module.verboseName,
                    value: module
                })
            }
            prompt([{
                type: 'checkbox',
                name: queryId,
                message: 'welcome to module app configurator!',
                choices: choices
            }]).then(answer=>{
                let resultPromises = [];
                answer = answer[queryId];

                for(let manifest of answer){
                    resultPromises.push(new Promise(resolve=>{
                        moduleLoader.loadModule(manifest.uuid).then(
                            result=>{
                                manifest.moduleConstructor = result;
                                resolve(manifest);
                            }
                        )
                    }));
                }
                Promise.all(resultPromises).then(
                    result=>{
                        exit(answer)
                    }
                )

            });
        }
    );
}